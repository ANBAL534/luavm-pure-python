import copy
import datetime
import gc
import inspect
import math
import os
import platform
import random
import struct
import subprocess
import sys
import time

from pympler.asizeof import asizeof

import luapatt


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls.__name__ not in cls._instances:
            # Using cls.__name__ instead of plain cls is a botched job, but makes the tests pass.
            # For the time being just make sure not to call classes in different modules the same
            cls._instances[cls.__name__] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls.__name__]


class LuaError(Exception):
    pass


class _LuaIPairIterator(object):
    def __init__(self, t):
        self._i = 0
        self._t = t

    def iterate(self):
        try:
            self._i += 1
            val = self._t[self._i]
            if not isinstance(val.value, _LuaNil):
                return _LuaValue(self._i), val
        except KeyError:
            return _LuaValue(_LuaNil())


class _LuaPairIterator(object):
    def __init__(self, t):
        self._t = t
        self._iterator = iter(t.items())

    def iterate(self):
        try:
            return next(self._iterator)
        except StopIteration:
            return _LuaValue(_LuaNil())


class _LuaGMatchIterator(object):
    def __init__(self, t):
        self._t = t
        self._iterator = iter(t)

    def iterate(self):
        try:
            return next(self._iterator)
        except StopIteration:
            return _LuaValue(_LuaNil())


class _LuaValue(object):
    def __init__(self, value):
        if isinstance(value, _LuaValue):
            raise ValueError("The value of a _LuaValue shouldn't be another _LuaValue")
        self.value = value

    def __str__(self):
        return str(self.value)


class BaseLib(object):
    # From http://underpop.free.fr/l/lua/#the_base_library
    # and https://www.capgo.com/Resources/SoftwareDev/LuaShortRef51.pdf

    @staticmethod
    def add_to_global(vm):
        for name, func in inspect.getmembers(BaseLib, predicate=inspect.isfunction):
            if name.startswith("lua_"):
                vm.g[name.replace("lua_", "")] = _LuaValue(getattr(BaseLib, name))

    @staticmethod
    def lua_print(vm, *args):
        # TODO Fix that when print is passed a function result as parameters it prints the whole stack
        print(*[str(v.value).rstrip('\x00') for v in args])

    @staticmethod
    def lua_setmetatable(vm, t, mt):
        raise NotImplementedError()

    @staticmethod
    def lua_getmetatable(vm, t):
        raise NotImplementedError()

    @staticmethod
    def lua_rawget(vm, t, i):
        raise NotImplementedError()

    @staticmethod
    def lua_rawset(vm, t, i, v):
        raise NotImplementedError()

    @staticmethod
    def lua_rawequal(vm, t1, t2):
        raise NotImplementedError()

    @staticmethod
    def lua_getfenv(vm, f=None):
        raise NotImplementedError()

    @staticmethod
    def lua_setfenv(vm, f, t):
        raise NotImplementedError()

    @staticmethod
    def lua_require(vm, pkgname):
        raise NotImplementedError()

    @staticmethod
    def lua_module(vm, name, *args):
        raise NotImplementedError()

    @staticmethod
    def lua_dofile(vm, filename=None):
        raise NotImplementedError()

    @staticmethod
    def lua_load(vm, func, chunkname=None):
        raise NotImplementedError()

    @staticmethod
    def lua_loadfile(vm, filename):
        raise NotImplementedError()

    @staticmethod
    def lua_loadstring(vm, s, name=None):
        raise NotImplementedError()

    @staticmethod
    def lua_loadlib(vm, library, func):
        raise NotImplementedError()

    @staticmethod
    def lua_pcall(vm, f, *args):
        if not callable(f.value):
            # Put the given register range the firsts on the stack (aka set up the parameters)
            for i, arg in enumerate(args):
                f.value["r"][i] = arg  # Remember: arg is a _LuaValue already

            try:
                vm.execute_function(function=f.value)
                ret = f.value["return_values"]
            except Exception as e:
                return [_LuaValue(0), _LuaValue(str(e) + "\x00")]  # TODO Return a real/valid lua error
        else:
            try:
                # Call of another stdlib function
                ret = f.value(*args)
            except Exception as e:
                return [_LuaValue(0), _LuaValue(str(e) + "\x00")]  # TODO Return a real/valid lua error
        return [_LuaValue(1), *list(ret)]

    @staticmethod
    def lua_xpcall(vm, f, h):
        try:
            if not callable(f.value):
                vm.execute_function(function=f.value)
                ret = f.value["return_values"]
            else:
                # Call of another stdlib function
                ret = f.value(vm)
        except Exception:
            if not callable(h.value):
                vm.execute_function(function=h.value)
                return [_LuaValue(0), *list(h.value["return_values"])]
            else:
                return [_LuaValue(0), *list(h.value(vm))]
        return [_LuaValue(1), *list(ret)]

    @staticmethod
    def lua_error(vm, msg, n=None):
        # TODO Print a custom traceback using function debug data
        raise LuaError(str(msg.value))

    @staticmethod
    def lua_assert(vm, v, msg=None):
        if not v.value:
            if msg is None:
                msg = _LuaValue("assertion failed!")
            BaseLib.lua_error(vm, msg)

    @staticmethod
    def lua_select(vm, index, *args):
        if isinstance(index.value, float) or isinstance(index.value, int):
            return list(args)[int(index.value)+1:]
        elif index.value == "#":
            return len(args)
        else:
            return _LuaValue(_LuaNil())

    @staticmethod
    def lua_type(vm, x):
        if isinstance(x.value, str):
            return [_LuaValue("string")]
        elif isinstance(x.value, float) or isinstance(x.value, int):
            return [_LuaValue("number")]
        elif isinstance(x.value, bool):
            return [_LuaValue("boolean")]
        elif isinstance(x.value, dict):
            if x.value.get("_ISFUNCTION", False):
                return [_LuaValue("function")]
            else:
                return [_LuaValue("table")]
        else:
            return [_LuaValue("nil")]

    @staticmethod
    def lua_tostring(vm, x):
        # TODO We should take into account how basic lua objects get to string
        return [str(x.value)]

    @staticmethod
    def lua_tonumber(vm, x, b=None):
        if b is None:
            b = _LuaValue(10)
        try:
            return float(int(x.value, b.value))
        except ValueError:
            return _LuaValue(_LuaNil())

    @staticmethod
    def lua_unpack(vm, t):
        sl = sorted(t.value.keys())
        ret = []
        for s in sl:
            ret.append(s)
        return _LuaValue(ret)

    @staticmethod
    def lua_collectgarbage(vm, option, v=None):
        if option == "stop":
            gc.disable()
        elif option == "restart":
            gc.enable()
        elif option == "collect":
            gc.collect()
        elif option == "count":
            return [_LuaValue(vm.get_memory_usage())]
        elif option == "step":
            raise NotImplementedError()
        elif option == "setpause":
            raise NotImplementedError()
        elif option == "setstepmul":
            raise NotImplementedError()

    @staticmethod
    def lua_ipairs(vm, t):
        return [_LuaIPairIterator(t.value)]

    @staticmethod
    def lua_pairs(vm, t):
        return [_LuaPairIterator(t.value)]

    @staticmethod
    def lua_next(vm, t, index=None):
        if index is None or isinstance(index.value, _LuaNil):
            index = _LuaValue(0)
        try:
            return [_LuaValue(index.value + 1), _LuaValue(t.value[index.value + 1])]
        except KeyError:
            try:
                return [_LuaValue(index.value + 1), _LuaValue(t.values()[index + 1])]
            except IndexError:
                return [_LuaValue(_LuaNil())]


class MathLib(object):
    @staticmethod
    def add_to_global(vm):
        vm.g["math"] = _LuaValue({
            "huge": _LuaValue(2**31-1),  # 32b max size
            "pi": _LuaValue(math.pi),
            "e": _LuaValue(math.e),
            "_RANDOMSEED": _LuaValue(int(time.time()))  # Default seed for random
        })
        for name, func in inspect.getmembers(MathLib, predicate=inspect.isfunction):
            if name.startswith("lua_"):
                vm.g["math"].value[name.replace("lua_", "")] = _LuaValue(getattr(MathLib, name))
        random.seed(vm.g["math"].value["_RANDOMSEED"])

    @staticmethod
    def lua_abs(vm, x):
        return [_LuaValue(math.fabs(x.value))]

    @staticmethod
    def lua_fmod(vm, x, y):
        return [_LuaValue(math.fmod(x.value, y.value))]

    @staticmethod
    def lua_floor(vm, x):
        return [_LuaValue(math.floor(x.value))]

    @staticmethod
    def lua_ceil(vm, x):
        return [_LuaValue(math.ceil(x.value))]

    @staticmethod
    def lua_min(vm, *args):
        return [_LuaValue(min([a.value for a in args]))]

    @staticmethod
    def lua_max(vm, *args):
        return [_LuaValue(max([a.value for a in args]))]

    @staticmethod
    def lua_modf(vm, x):
        return [_LuaValue(a) for a in math.modf(x.value)]

    @staticmethod
    def lua_sqrt(vm, x):
        return [_LuaValue(math.sqrt(x.value))]

    @staticmethod
    def lua_pow(vm, x, y):
        return [_LuaValue(math.pow(x.value, y.value))]

    @staticmethod
    def lua_exp(vm, x):
        return [_LuaValue(math.exp(x.value))]

    @staticmethod
    def lua_log(vm, x):
        return [_LuaValue(math.log(x.value, math.e))]

    @staticmethod
    def lua_log10(vm, x):
        return [_LuaValue(math.log(x.value, 10))]

    @staticmethod
    def lua_frexp(vm, x):
        return [_LuaValue(math.frexp(x.value))]

    @staticmethod
    def lua_ldexp(vm, x, y):
        return [_LuaValue(math.ldexp(x.value, y.value))]

    @staticmethod
    def lua_deg(vm, a):
        return [_LuaValue(math.degrees(a.value))]

    @staticmethod
    def lua_rad(vm, a):
        return [_LuaValue(math.radians(a.value))]

    @staticmethod
    def lua_sin(vm, a):
        return [_LuaValue(math.sin(a.value))]

    @staticmethod
    def lua_cos(vm, a):
        return [_LuaValue(math.cos(a.value))]

    @staticmethod
    def lua_tan(vm, a):
        return [_LuaValue(math.tan(a.value))]

    @staticmethod
    def lua_asin(vm, a):
        return [_LuaValue(math.asin(a.value))]

    @staticmethod
    def lua_acos(vm, a):
        return [_LuaValue(math.acos(a.value))]

    @staticmethod
    def lua_atan(vm, a):
        return [_LuaValue(math.atan(a.value))]

    @staticmethod
    def lua_random(vm, n=None, m=None):
        if n is None and m is None:
            n = _LuaValue(0)
            m = _LuaValue(1)
        elif m is None:
            m = n
            n = _LuaValue(1)

        # Workaround for deterministic random sequences when multiple Lua VMs are running in parallel in the same
        # Python instance
        vm.g["math"]["_RANDOMSEED"].value += 1
        random.seed(vm.g["math"]["_RANDOMSEED"].value)
        return [_LuaValue(random.uniform(n.value, m.value))]

    @staticmethod
    def lua_randomseed(vm, n):
        vm.g["math"]["_RANDOMSEED"] = _LuaValue(int(n.value))


class TableLib(object):
    @staticmethod
    def add_to_global(vm):
        vm.g["table"] = _LuaValue({})
        for name, func in inspect.getmembers(TableLib, predicate=inspect.isfunction):
            if name.startswith("lua_"):
                vm.g["table"].value[name.replace("lua_", "")] = _LuaValue(getattr(TableLib, name))

    @staticmethod
    def lua_insert(vm, table, i=None, v=None):
        if i is not None and v is None:
            v = i
            i = _LuaValue(len(table.value)+1)  # Remember: Lua arrays start at 1, so len(table) has the last element
        table.value[i.value] = v

    @staticmethod
    def lua_remove(vm, table, i=None):
        if i is None:
            i = _LuaValue(len(table.value))
        return [table.value.pop(i.value, _LuaValue(_LuaNil()))]

    @staticmethod
    def lua_maxn(vm, table):
        n = 0
        for i in range(1, len(table.value)):
            if i not in table.value or isinstance(table.value[i], _LuaNil):
                break
            n += 1
        return [_LuaValue(n)]

    @staticmethod
    def lua_sort(vm, table, cf=None):
        def default_compare_function(item1, item2):
            if float(item1) < float(item2):
                return -1
            elif float(item1) > float(item2):
                return 1
            else:
                return 0

        def custom_compare_function(item1, item2):
            # Put the given register range the firsts on the stack (aka set up the parameters)
            cf.value["r"].setdefault(0, _LuaValue(None)).value = float(item1)
            cf.value["r"].setdefault(1, _LuaValue(None)).value = float(item2)
            vm.execute_function(cf.value)
            if cf.value["return_values"][0].value == 0:  # item1 < item2
                return -1
            elif cf.value["return_values"][0].value != 0:  # item1 > item2
                return 1
            return 0

        if cf is None:
            kf = default_compare_function
        else:
            kf = custom_compare_function

        i = 1
        j = TableLib.lua_maxn(vm, table)[0].value
        sorted_values = sorted(table.value.values(), key=kf)

        for w in range(i, j+1):
            table.value[w] = sorted_values[w]

        return [table]

    @staticmethod
    def lua_concat(vm, table, string=None, i=None, j=None):
        if string is None:
            string = _LuaValue("")
        if i is None:
            i = _LuaValue(1)
        if j is None:
            j = TableLib.lua_maxn(vm, table)[0]

        if len(table.value) == 0 or i.value > j.value:
            return [_LuaValue("")]

        ret = str(table.value[i.value].value)
        for w in range(i.value+1, j.value+1):
            ret += string.value + str(table.value[w].value)
        return [_LuaValue(ret)]


class OSLib(object):
    @staticmethod
    def add_to_global(vm):
        vm.g["os"] = _LuaValue({})
        for name, func in inspect.getmembers(OSLib, predicate=inspect.isfunction):
            if name.startswith("lua_"):
                vm.g["os"].value[name.replace("lua_", "")] = _LuaValue(getattr(OSLib, name))

    @staticmethod
    def lua_time(vm, tt=None):
        if tt is None:
            dt = datetime.datetime.now()
            tt = _LuaValue({
                "year": _LuaValue(dt.year),
                "month": _LuaValue(dt.month),
                "day": _LuaValue(dt.day),
                "hour": _LuaValue(dt.hour),
                "min": _LuaValue(dt.minute),
                "sec": _LuaValue(dt.second),
                "isdst": _LuaValue(dt.dst())
            })
        if "hour" not in tt.value:
            tt.value["hour"] = _LuaValue(0)
        if "min" not in tt.value:
            tt.value["min"] = _LuaValue(0)
        if "sec" not in tt.value:
            tt.value["sec"] = _LuaValue(0)
        dt = datetime.datetime(year=tt.value["year"].value, month=tt.value["month"].value, day=tt.value["day"].value, 
                               hour=tt.value["hour"].value, minute=tt.value["min"].value, second=tt.value["sec"].value)
        return [_LuaValue(dt.timestamp())]
    
    @staticmethod
    def lua_difftime(vm, t2, t1):
        if "hour" not in t1.value:
            t1.value["hour"] = _LuaValue(0)
        if "min" not in t1.value:
            t1.value["min"] = _LuaValue(0)
        if "sec" not in t1.value:
            t1.value["sec"] = _LuaValue(0)
        dt1 = datetime.datetime(year=t1.value["year"].value, month=t1.value["month"].value, day=t1.value["day"].value,
                                hour=t1.value["hour"].value, minute=t1.value["min"].value, second=t1.value["sec"].value)
        if "hour" not in t2.value:
            t2.value["hour"] = _LuaValue(0)
        if "min" not in t2.value:
            t2.value["min"] = _LuaValue(0)
        if "sec" not in t2.value:
            t2.value["sec"] = _LuaValue(0)
        dt2 = datetime.datetime(year=t2.value["year"].value, month=t2.value["month"].value, day=t2.value["day"].value,
                                hour=t2.value["hour"].value, minute=t2.value["min"].value, second=t2.value["sec"].value)
        return [_LuaValue((dt2-dt1).total_seconds())]

    @staticmethod
    def lua_date(vm, fmt=None, t=None):
        if t is None:
            if fmt is not None and fmt.value.startswith("!"):
                dt = datetime.datetime.now(tz=datetime.timezone.utc)
                fmt.value = fmt.value.replace("!", "")
            else:
                dt = datetime.datetime.now()
            t = _LuaValue({
                "year": _LuaValue(dt.year),
                "month": _LuaValue(dt.month),
                "day": _LuaValue(dt.day),
                "hour": _LuaValue(dt.hour),
                "min": _LuaValue(dt.minute),
                "sec": _LuaValue(dt.second),
                "isdst": _LuaValue(dt.dst())
            })
        if fmt is None:
            fmt = _LuaValue("*t")

        if "*t" in fmt.value:
            return [t]

        if "hour" not in t.value:
            t.value["hour"] = _LuaValue(0)
        if "min" not in t.value:
            t.value["min"] = _LuaValue(0)
        if "sec" not in t.value:
            t.value["sec"] = _LuaValue(0)
        dt = datetime.datetime(year=t.value["year"].value, month=t.value["month"].value, day=t.value["day"].value,
                               hour=t.value["hour"].value, minute=t.value["min"].value, second=t.value["sec"].value)

        return [_LuaValue(dt.strftime(fmt))]

    @staticmethod
    def lua_clock(vm):
        # TODO Not real CPU time but close enough for now
        return [_LuaValue(time.time() - vm.start_time)]

    @staticmethod
    def lua_execute(vm, string):
        return [_LuaValue(os.system(string.value))]

    @staticmethod
    def lua_exit(vm, code=None):
        if code is None:
            code = _LuaValue(0)
        vm.exit = code.value

    @staticmethod
    def lua_getenv(vm, variable):
        ret = os.getenv(str(variable.value))
        if ret is None:
            return [_LuaNil()]
        return [_LuaValue(ret)]

    @staticmethod
    def lua_setlocale(vm, string, category=None):
        if category is None:
            category = _LuaValue("all")
        raise NotImplementedError()

    @staticmethod
    def lua_remove(vm, file):
        try:
            try:
                os.remove(file.value)
            except OSError:
                os.rmdir(file.value)
        except Exception as e:
            return [_LuaValue(_LuaNil()), _LuaValue(str(e))]

    @staticmethod
    def lua_rename(vm, file1, file2):
        try:
            os.renames(file1.value, file2.value)
        except Exception as e:
            return [_LuaValue(_LuaNil()), _LuaValue(str(e))]

    @staticmethod
    def lua_tmpname(vm):
        raise NotImplementedError()


class StringLib(object):
    @staticmethod
    def add_to_global(vm):
        vm.g["string"] = _LuaValue({})
        for name, func in inspect.getmembers(StringLib, predicate=inspect.isfunction):
            if name.startswith("lua_"):
                vm.g["string"].value[name.replace("lua_", "")] = _LuaValue(getattr(StringLib, name))

    @staticmethod
    def lua_len(vm, string):
        return [len(str(string.value))]

    @staticmethod
    def lua_sub(vm, string, i, j=None):
        if j is None:
            j = _LuaValue(len(str(string.value)))
        return [_LuaValue(''.join(list(str(string.value))[i.value-1:j.value]))]

    @staticmethod
    def lua_rep(vm, string, n):
        ret = ""
        for _ in range(int(n.value)):
            ret += str(string.value)
        return [_LuaValue(ret)]

    @staticmethod
    def lua_upper(vm, string):
        return [_LuaValue(str(string.value).upper())]

    @staticmethod
    def lua_lower(vm, string):
        return [_LuaValue(str(string.value).lower())]

    @staticmethod
    def lua_reverse(vm, string):
        return [_LuaValue(str(string.value)[::-1])]

    @staticmethod
    def lua_byte(vm, string, i=None):
        if i is None:
            i = _LuaValue(1)
        j = i.value - 1
        return [_LuaValue(ord(list(string.value)[j]))]

    @staticmethod
    def lua_char(vm, *args):
        chars = []
        for arg in args:
            chars.append(chr(arg.value))
        return [_LuaValue(''.join(chars))]

    @staticmethod
    def lua_format(vm, string, *args):
        # TODO Check for special cases with the lua formatter against the python one (ex.: %q flag)
        return [_LuaValue(string.value % (arg.value for arg in args))]

    @staticmethod
    def lua_find(vm, string, pattern, i=None, d=None):
        if i is None:
            i = _LuaValue(1)
        if d is None:
            d = _LuaValue(True)
        ret = luapatt.find(string.value, pattern.value, i.value-1, d.value)
        if ret is None:
            return _LuaValue(_LuaNil())
        # TODO Check what is returned when multiple matches are found
        return [*[_LuaValue(r) for r in ret]]

    @staticmethod
    def lua_gmatch(vm, string, pattern):
        return [_LuaGMatchIterator(luapatt.gmatch(string.value, pattern.value))]

    @staticmethod
    def lua_match(vm, string, pattern, i=None):
        if i is None:
            i = _LuaValue(1)
        ret = luapatt.match(string.value, pattern.value, i.value-1)
        if ret is None:
            return _LuaValue(_LuaNil())
        if isinstance(ret, str):
            return [_LuaValue(ret)]
        return [*[_LuaValue(r) for r in ret]]

    @staticmethod
    def lua_gsub(vm, string, pattern, r, n=None):
        if n is None:
            n = _LuaValue(1)
        if isinstance(r.value, str):
            return [*[_LuaValue(a) for a in luapatt.gsub(string.value, pattern.value, r.value, n.value, True)]]
        elif isinstance(r.value, dict) and r.value.get("_ISFUNCTION", False):
            dict_key = luapatt.match(string.value, pattern.value)
            return [*[_LuaValue(a) for a in luapatt.gsub(string.value, pattern.value, r.value[dict_key], n.value, True)]]
        elif isinstance(r.value, dict) and not r.value.get("_ISFUNCTION", False):
            def custom_compare_function(w):
                # Put the given register range the firsts on the stack (aka set up the parameters)
                r.value["r"][0].value = w
                vm.execute_function(r.value)
                if isinstance(r.value["return_values"][0].value, _LuaNil):
                    return w
                return r["return_values"][0].value
            return [*[_LuaValue(a) for a in luapatt.gsub(string.value, pattern.value, custom_compare_function, n.value, True)]]

    @staticmethod
    def lua_dump(vm, function):
        raise NotImplementedError()


class Instruction(object):
    def __init__(self, byte_list):
        self.instruction_bytes = b''.join(byte_list)
        self.instruction_bits = "{0:32b}".format(int.from_bytes(self.instruction_bytes, byteorder=sys.byteorder))\
            .replace(' ', '0')
        self.op = int(self.instruction_bits[-6:], 2)
        self.op_name = VirtualMachine.lua_opcodes[self.op]
        self.a = int(self.instruction_bits[-6 - 8:-6], 2)
        self.c = int(self.instruction_bits[-9 - 8 - 6:-6 - 8], 2)
        self.b = int(self.instruction_bits[:9], 2)
        self.bx = int(self.instruction_bits[:18], 2)
        self.sbx = int(self.instruction_bits[:18], 2) - 131071

    def __str__(self):
        d = copy.copy(self.__dict__)
        d.pop("instruction_bytes", None)
        d.pop("instruction_bits", None)
        return str(d)


class Constant(object):
    def __init__(self, type_of_constant, value):
        self.type_of_constant = type_of_constant
        self.value = value


class LocalDebug(object):
    def __init__(self, name, start_pc_scope, end_pc_scope):
        self.name = name
        self.start_pc_scope = start_pc_scope
        self.end_pc_scope = end_pc_scope


class _LuaNil(metaclass=Singleton):
    def __str__(self):
        return "nil"

    def __eq__(self, other):
        if type(self) == type(other):
            return True
        return False

    def __bool__(self):
        return False


class VmError(Exception):
    pass


class VirtualMachine(object):
    lua_signature_supported = b'\x1bLua'
    lua_version_supported = 81  # 5.1
    lua_format_supported = 0
    if sys.byteorder == 'little':
        endianness_supported = 1  # little endian
    else:
        endianness_supported = 0  # big endian
    size_of_int_supported = 4
    size_of_instruction_supported = 4
    if platform.architecture()[0] == '64bit':
        size_of_size_t_supported = 8  # x64 machines
    else:
        size_of_size_t_supported = 4  # x32 machines
    size_of_lua_number_supported = 8
    size_of_integral_supported = 0  # floating point
    fields_per_flush = 50
    max_stack_size = 250
    lua_constant_types = {
        "nil": 0,
        "boolean": 1,
        "number": 3,
        "string": 4
    }
    lua_opcodes = {
        0: "MOVE",
        1: "LOADK",
        2: "LOADBOOL",
        3: "LOADNIL",
        4: "GETUPVAL",
        5: "GETGLOBAL",
        6: "GETTABLE",
        7: "SETGLOBAL",
        8: "SETUPVAL",
        9: "SETTABLE",
        10: "NEWTABLE",
        11: "SELF",
        12: "ADD",
        13: "SUB",
        14: "MUL",
        15: "DIV",
        16: "MOD",
        17: "POW",
        18: "UNM",
        19: "NOT",
        20: "LEN",
        21: "CONCAT",
        22: "JMP",
        23: "EQ",
        24: "LT",
        25: "LE",
        26: "TEST",
        27: "TESTSET",
        28: "CALL",
        29: "TAILCALL",
        30: "RETURN",
        31: "FORLOOP",
        32: "FORPREP",
        33: "TFORLOOP",
        34: "SETLIST",
        35: "CLOSE",
        36: "CLOSURE",
        37: "VARARG",
    }

    def __init__(self, lua_bytecode_path=None, lua_bytecode=None, global_dict=None):
        if lua_bytecode_path is None and lua_bytecode is None:
            raise VmError("lua_bytecode_path and lua_bytecode can't be both None at the same time.")

        self._lua_bytecode = lua_bytecode
        if lua_bytecode_path is not None:
            self._lua_bytecode = self.read_bytecode_file(str(lua_bytecode_path))

        self.header = self.pop_header(self._lua_bytecode)

        if not self.is_valid_bytecode_header(self.header):
            raise VmError("Invalid Lua5.1 bytecode.")

        self._op_code_implementation = {
            "MOVE": self._op_move,
            "LOADK": self._op_loadk,
            "LOADBOOL": self._op_loadbool,
            "LOADNIL": self._op_loadnil,
            "GETUPVAL": self._op_getupval,
            "GETGLOBAL": self._op_getglobal,
            "GETTABLE": self._op_gettable,
            "SETGLOBAL": self._op_setglobal,
            "SETUPVAL": self._op_setupval,
            "SETTABLE": self._op_settable,
            "NEWTABLE": self._op_newtable,
            "SELF": self._op_self,
            "ADD": self._op_add,
            "SUB": self._op_sub,
            "MUL": self._op_mul,
            "DIV": self._op_div,
            "MOD": self._op_mod,
            "POW": self._op_pow,
            "UNM": self._op_unm,
            "NOT": self._op_not,
            "LEN": self._op_len,
            "CONCAT": self._op_concat,
            "JMP": self._op_jmp,
            "EQ": self._op_eq,
            "LT": self._op_lt,
            "LE": self._op_le,
            "TEST": self._op_test,
            "TESTSET": self._op_testset,
            "CALL": self._op_call,
            "TAILCALL": self._op_tailcall,
            "RETURN": self._op_return,
            "FORLOOP": self._op_forloop,
            "FORPREP": self._op_forprep,
            "TFORLOOP": self._op_tforloop,
            "SETLIST": self._op_setlist,
            "CLOSE": self._op_close,
            "CLOSURE": self._op_closure,
            "VARARG": self._op_vararg,
        }

        self._main_function = self.pop_function(self.header, self._lua_bytecode)

        if global_dict is None:
            self.g = {
                "_VERSION": "Lua 5.1",
            }  # Globals
            BaseLib.add_to_global(self)
            MathLib.add_to_global(self)
            TableLib.add_to_global(self)
            OSLib.add_to_global(self)
            StringLib.add_to_global(self)

        self.g["_G"] = _LuaValue(self.g)
        self.start_time = time.time()
        self.exit = None

    def get_memory_usage(self):
        return asizeof(self)

    def is_valid_bytecode_header(self, header):
        if header["signature"] != self.lua_signature_supported:
            return False
        if header["lua_version"] != self.lua_version_supported:
            return False
        if header["format_version"] != self.lua_format_supported:
            return False
        if header["endianness"] != self.endianness_supported:
            return False
        if header["size_of_int"] != self.size_of_int_supported:
            return False
        if header["size_of_size_t"] != self.size_of_size_t_supported:
            return False
        if header["size_of_instruction"] != self.size_of_instruction_supported:
            return False
        if header["size_of_number"] != self.size_of_lua_number_supported:
            return False
        if header["integral"] != self.size_of_integral_supported:
            return False
        return True

    def execute_instruction(self, instruction, function):
        function["pc"] += 1
        return self._op_code_implementation[self.lua_opcodes[instruction.op]](instruction, function)

    def execute_function(self, function=None):
        if function is None:
            function = self._main_function

        function["pc"] = 0

        is_returned = False
        while not is_returned:
            if self.exit is not None:
                break
            is_returned = self.execute_instruction(function["instructions"][function["pc"]], function)

        if self.exit is None:
            return 0
        return self.exit

    def _op_move(self, instruction, function):
        try:
            function["r"][instruction.a] = function["r"][instruction.b]
        except KeyError:
            function["r"][instruction.a] = _LuaValue(_LuaNil())
        return False

    def _op_loadnil(self, instruction, function):
        for i in range(instruction.a, instruction.b+1):
            function["r"][i] = _LuaValue(_LuaNil())
        return False

    def _op_loadk(self, instruction, function):
        try:
            function["r"][instruction.a] = _LuaValue(function["constants"][instruction.bx].value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(_LuaNil())
        return False

    def _op_loadbool(self, instruction, function):
        function["r"][instruction.a] = _LuaValue(instruction.b != 0)
        if instruction.c != 0:
            function["pc"] += 1
        return False

    def _op_getglobal(self, instruction, function):
        function["r"][instruction.a] = self.g[function["constants"][instruction.bx].value]
        return False

    def _op_setglobal(self, instruction, function):
        self.g[function["constants"][instruction.bx].value] = function["r"][instruction.a]
        return False

    def _op_getupval(self, instruction, function):
        function["r"][instruction.a] = function["upvalues"][instruction.b]
        return False

    def _op_setupval(self, instruction, function):
        function["upvalues"][instruction.b] = function["r"][instruction.a]
        return False

    def _op_gettable(self, instruction, function):
        try:
            function["r"][instruction.a] = function["r"][instruction.b].value[
                self._get_rk(instruction.c, function).value]
        except IndexError:
            function["r"][instruction.a] = _LuaValue(_LuaNil())
        except KeyError:
            function["r"][instruction.a] = _LuaValue(_LuaNil())
        return False

    def _op_settable(self, instruction, function):
        function["r"][instruction.a].value[self._get_rk(instruction.b, function).value] = \
            self._get_rk(instruction.c, function)
        return False

    def _op_newtable(self, instruction, function):
        function["r"][instruction.a] = _LuaValue({})  # TODO We are ignoring start sizes on the table creation, we are also ignoring the difference between array and hash table
        return False

    def _op_setlist(self, instruction, function):
        if instruction.c == 0:
            c = int.from_bytes(function["instructions"][function["pc"]].instruction_bytes,
                               byteorder=sys.byteorder, signed=False)
            function["pc"] += 1
        else:
            c = instruction.c
        if instruction.b > 0:
            stop = instruction.b + 1
        else:
            stop = VirtualMachine.max_stack_size
        for i in range(1, stop):
            function["r"][instruction.a].value[c - 1 + i] = function["r"][instruction.a + i]
        return False

    def _op_add(self, instruction, function):
        try:
            function["r"][instruction.a] = \
                _LuaValue(self._get_rk(instruction.b, function).value + self._get_rk(instruction.c, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(
                self._get_rk(instruction.b, function).value + self._get_rk(instruction.c, function).value)
        return False

    def _op_sub(self, instruction, function):
        try:
            function["r"][instruction.a] = \
                _LuaValue(self._get_rk(instruction.b, function).value - self._get_rk(instruction.c, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(
                self._get_rk(instruction.b, function).value - self._get_rk(instruction.c, function).value)
        return False

    def _op_mul(self, instruction, function):
        try:
            function["r"][instruction.a] = \
                _LuaValue(self._get_rk(instruction.b, function).value * self._get_rk(instruction.c, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(
                self._get_rk(instruction.b, function).value * self._get_rk(instruction.c, function).value)
        return False

    def _op_div(self, instruction, function):
        try:
            function["r"][instruction.a] = \
                _LuaValue(self._get_rk(instruction.b, function).value / self._get_rk(instruction.c, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(
                self._get_rk(instruction.b, function).value / self._get_rk(instruction.c, function).value)
        return False

    def _op_mod(self, instruction, function):
        try:
            function["r"][instruction.a] = \
                _LuaValue(self._get_rk(instruction.b, function).value % self._get_rk(instruction.c, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(
                self._get_rk(instruction.b, function).value % self._get_rk(instruction.c, function).value)
        return False

    def _op_pow(self, instruction, function):
        try:
            function["r"][instruction.a] = \
                _LuaValue(self._get_rk(instruction.b, function).value ** self._get_rk(instruction.c, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(
                self._get_rk(instruction.b, function).value ** self._get_rk(instruction.c, function).value)
        return False

    def _op_unm(self, instruction, function):
        try:
            function["r"][instruction.a] = _LuaValue(-self._get_rk(instruction.b, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(-self._get_rk(instruction.b, function).value)
        return False

    def _op_not(self, instruction, function):
        try:
            function["r"][instruction.a] = _LuaValue(not self._get_rk(instruction.b, function).value)
        except KeyError:
            function["r"][instruction.a] = _LuaValue(not self._get_rk(instruction.b, function).value)
        return False

    def _op_len(self, instruction, function):
        # TODO We might run into problems with how lua considers array lengths (2.5.5 in https://www.lua.org/manual/5.1/es/manual.html)
        if isinstance(function["r"][instruction.b].value, str):
            function["r"][instruction.a] = _LuaValue(len(function["r"][instruction.b].value))
        else:
            function["r"][instruction.a] = TableLib.lua_maxn(self, function["r"][instruction.b])[0]
        return False

    def _op_concat(self, instruction, function):
        function["r"][instruction.a] = function["r"][instruction.b]
        for i in range(instruction.b + 1, instruction.c + 1):
            function["r"][instruction.a].value += function["r"][i].value
        return False

    def _op_jmp(self, instruction, function):
        function["pc"] += instruction.sbx
        return False

    def _op_eq(self, instruction, function):
        if (self._get_rk(instruction.b, function).value == self._get_rk(instruction.c, function).value) \
                != (instruction.a != 0):
            function["pc"] += 1
        return False

    def _op_lt(self, instruction, function):
        if (self._get_rk(instruction.b, function).value < self._get_rk(instruction.c, function).value) \
                != (instruction.a != 0):
            function["pc"] += 1
        return False

    def _op_le(self, instruction, function):
        if (self._get_rk(instruction.b, function).value <= self._get_rk(instruction.c, function).value) \
                != (instruction.a != 0):
            function["pc"] += 1
        return False

    def _op_testset(self, instruction, function):
        if isinstance(function["r"][instruction.b].value, int):
            function["r"][instruction.b] = _LuaValue(function["r"][instruction.b].value != 0)

        if function["r"][instruction.b].value == (instruction.c != 0):
            function["pc"] += 1
        else:
            function["r"][instruction.a] = function["r"][instruction.b]
        return False

    def _op_test(self, instruction, function):
        if isinstance(function["r"][instruction.a].value, int):
            function["r"][instruction.a] = _LuaValue(function["r"][instruction.a].value != 0)

        if function["r"][instruction.a].value == (instruction.c != 0):
            function["pc"] += 1
        return False

    def _op_closure(self, instruction, function):
        # We need to start fresh in a closure
        closure = copy.deepcopy(function["functions"][instruction.bx])

        # Set-up upvalues
        for i in range(closure["n_upvalues"]):
            if self.lua_opcodes[function["instructions"][function["pc"]].op] == "GETUPVAL":
                closure["upvalues"][i] = function["upvalues"][function["instructions"][function["pc"]].b]
            else:
                closure["upvalues"][i] = function["r"][function["instructions"][function["pc"]].b]
            function["pc"] += 1  # Skip the used pseudo-instructions for the upvalues

        function["r"][instruction.a] = _LuaValue(closure)
        return False

    def _op_close(self, instruction, function):
        for i in range(VirtualMachine.max_stack_size):
            if instruction.a + i in function["r"]:
                function["r"][instruction.a + i] = _LuaValue(_LuaNil())
        return False

    def _op_call(self, instruction, function):
        closure = function["r"][instruction.a].value

        if instruction.b == 1:  # No parameters
            r = range(0)
        elif instruction.b > 1:  # b-1 parameters
            r = range(instruction.a + 1, instruction.a + instruction.b)
        else:  # if instruction.b == 0:  # indeterminate parameter number (send all the stack from a+1 )
            r = range(instruction.a + 1, len(function["r"]))

        if not callable(closure):
            # Regular user made lua function

            # Put the given register range the firsts on the stack (aka set up the parameters)
            r_index = 0
            for i in r:
                closure["r"][r_index] = function["r"][i]
                closure["varargs"].append(function["r"][i])
                r_index += 1

            # "Freeze" upvalues
            for i in range(len(closure["upvalues"])):
                closure["upvalues"][i] = copy.copy(closure["upvalues"][i])

            self.execute_function(function=closure)

            ret = closure["return_values"]
        else:
            # Call of a stdlib function
            ret = closure(self, *[function["r"][i] for i in r])

        # Set up return values if any
        if isinstance(ret, list):
            ret_index = instruction.a
            for r in ret:
                # TODO check with the function how many return objects are expected to nil the remaining
                #  instead of counting on the _LuaNil on KeyError
                function["r"][ret_index] = r
                ret_index += 1

        return False

    def _op_return(self, instruction, function):
        if instruction.b == 1:
            stop = -1
        elif instruction.b > 1:
            stop = instruction.a + instruction.b - 1
            function["return_values"] = []
        else:  # instruction.b == 0
            stop = len(function["r"].keys())
            function["return_values"] = []

        for i in range(instruction.a, stop):
            function["return_values"].append(function["r"][i])

        function["r"] = {}  # Clean the stack before returning

        self._op_close(instruction, function)
        return True

    def _op_tailcall(self, instruction, function):
        # TODO Implement tailcall optimization to reduce function nesting
        return self._op_call(instruction, function)

    def _op_vararg(self, instruction, function):
        if instruction.b > 1:
            r = range(instruction.a, instruction.a + instruction.b)
        else:  # if instruction.b == 0:  # indeterminate parameter number (send all the stack from a)
            r = range(instruction.a, VirtualMachine.max_stack_size)

        for i, arg in enumerate(function["varargs"]):
            if instruction.a + i not in r:
                break
            function["r"][instruction.a + i] = arg

        return False

    def _op_self(self, instruction, function):
        function["r"][instruction.a + 1] = function["r"][instruction.b]
        function["r"][instruction.a] = function["r"][instruction.b].value[self._get_rk(instruction.c, function).value]
        return False

    def _op_forprep(self, instruction, function):
        function["r"][instruction.a].value -= function["r"][instruction.a + 2].value
        function["pc"] += instruction.sbx
        return False

    def _op_forloop(self, instruction, function):
        function["r"][instruction.a] = _LuaValue(function["r"][instruction.a + 2].value
                                                 + function["r"][instruction.a].value)
        function["r"][instruction.a].value = min(function["r"][instruction.a].value,
                                                 function["r"][instruction.a + 1].value + 1)
        if function["r"][instruction.a].value <= function["r"][instruction.a + 1].value:
            function["pc"] += instruction.sbx
            function["r"][instruction.a + 3] = function["r"][instruction.a]
            return False
        return False

    def _op_tforloop(self, instruction, function):
        # TODO Should we wrap lua iterators in a _LuaValue ?
        lua_iterator = function["r"][instruction.a]
        ret = lua_iterator.iterate()
        if isinstance(ret.value, _LuaNil):
            function["pc"] += 1
        else:
            function["r"][instruction.a + 3] = ret[0]
            function["r"][instruction.a + 4] = ret[1]
        return False

    def _get_rk(self, int_from_instruction_field, function):
        if int_from_instruction_field >= 256:
            # Look in constants
            return _LuaValue(function["constants"][int_from_instruction_field - 256].value)
        else:
            # Look in r
            return function["r"].get(int_from_instruction_field, _LuaValue(_LuaNil()))

    @staticmethod
    def read_bytecode_file(path):
        bytes_list = []
        with open(path, "rb") as f:
            byte = f.read(1)
            while byte:
                bytes_list.append(byte)
                byte = f.read(1)
        return bytes_list

    @staticmethod
    def pop_header(bytes_list):
        header = {
            "signature": b''.join([bytes_list.pop(0) for _ in range(4)]),
            "lua_version": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
            "format_version": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
            "endianness": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
            "size_of_int": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
            "size_of_size_t": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
            "size_of_instruction": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
            "size_of_number": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
            "integral": int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False),
        }
        return header

    @staticmethod
    def pop_function(header, bytes_list):
        name_len = int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_size_t"])]),
                                  byteorder=sys.byteorder, signed=False)
        name = b''.join([bytes_list.pop(0) for _ in range(name_len)]).decode("utf-8")

        first_line = int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]),
                                    byteorder=sys.byteorder, signed=False)
        last_line = int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]),
                                   byteorder=sys.byteorder, signed=False)
        n_upvalues = int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False)
        n_parameters = int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False)
        is_vararg = int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False)
        max_stack_size = int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False)

        instructions_len = int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]),
                                          byteorder=sys.byteorder, signed=False)
        instructions = [Instruction(
            [bytes_list.pop(0) for _ in range(header["size_of_instruction"])]
        ) for _ in range(instructions_len)]

        constants_len = int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]),
                                          byteorder=sys.byteorder, signed=False)
        constants = [VirtualMachine.pop_constant(header, bytes_list) for _ in range(constants_len)]

        functions_len = int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]),
                                       byteorder=sys.byteorder, signed=False)
        functions = [VirtualMachine.pop_function(header, bytes_list) for _ in range(functions_len)]

        function = {
            "name": name,
            "first_line": first_line,
            "last_line": last_line,
            "n_upvalues": n_upvalues,
            "n_parameters": n_parameters,
            "is_vararg": is_vararg,
            "max_stack_size": max_stack_size,
            "instructions": instructions,
            "constants": constants,
            "functions": functions,
            "upvalues": {},  # UpValues are added on runtime
            "r": {},  # The function's stack
            "varargs": [],  # Parameters passed to the function in case of a later vararg op call
            "return_values": _LuaValue(_LuaNil()),
            "pc": 0,
            "_ISFUNCTION": True
        }
        try:
            debug_function = {
                # Debug lists, can be stripped.
                "source_line_positions_debug": [int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]), byteorder=sys.byteorder, signed=False) for _ in range(int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]), byteorder=sys.byteorder, signed=False))],
                "locals_debug": [LocalDebug(
                    name=b''.join([bytes_list.pop(0) for _ in range(int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_size_t"])]), byteorder=sys.byteorder, signed=False))]).decode("utf-8"),
                    start_pc_scope=int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]), byteorder=sys.byteorder, signed=False),
                    end_pc_scope=int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]), byteorder=sys.byteorder, signed=False)
                ) for _ in range(int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]), byteorder=sys.byteorder, signed=False))],
                "upvalues_debug": [b''.join([bytes_list.pop(0) for _ in range(int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_size_t"])]), byteorder=sys.byteorder, signed=False))]).decode("utf-8") for _ in range(int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_int"])]), byteorder=sys.byteorder, signed=False))]
            }
        except IndexError:
            debug_function = {}

        function = {**function, **debug_function}
        return function

    @staticmethod
    def pop_constant(header, bytes_list):
        type_of_constant = int.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False)
        if type_of_constant == VirtualMachine.lua_constant_types["nil"]:
            return Constant(type_of_constant, _LuaNil())
        if type_of_constant == VirtualMachine.lua_constant_types["boolean"]:
            return Constant(type_of_constant, bool.from_bytes(bytes_list.pop(0), byteorder=sys.byteorder, signed=False))  # TODO BOOLEAN type 0 or 1 don't know if needed to read a byte or a bit (or an integer!)
        if type_of_constant == VirtualMachine.lua_constant_types["number"]:
            return Constant(type_of_constant, struct.unpack('d', b''.join([bytes_list.pop(0) for _ in range(header["size_of_number"])]))[0])
        if type_of_constant == VirtualMachine.lua_constant_types["string"]:
            string_len = int.from_bytes(b''.join([bytes_list.pop(0) for _ in range(header["size_of_size_t"])]),
                                        byteorder=sys.byteorder, signed=False)
            return Constant(type_of_constant, b''.join([bytes_list.pop(0) for _ in range(string_len)]).decode("utf-8")
                            .rstrip('\x00'))
        raise VmError("Bytes list didn't contain a valid constant.")


def main():
    bashCmd = ["luac5.1", "-o", "lua_files_test/luac.out", "lua_files_test/simple.lua"]
    process = subprocess.Popen(bashCmd, stdout=subprocess.PIPE)
    process.wait()

    vm = VirtualMachine(lua_bytecode_path="lua_files_test/luac.out")
    vm.execute_function()


if __name__ == '__main__':
    main()
