a = {
    A = 5,
    get_A = function(self) return self.A end
}
local b = a
local c = b

local d = {"f", "oo"}

local one = 1
local four = 4
local two = 2
local three_mod = #(d[1]..d[2])

local tr, fls = true, false
local res = nil

function f1(...)
    local fa, fb, fc = ...
    if fc == nil and fa > -one and not fb <= 0 then
        for i=1,3 do
            fa = fa + fb + (a:get_A() - (((one * four) / two) ^ (two % three_mod)))
            three_mod = three_mod + 1
            print("fa=", fa)
        end
        return fa
    end
end

function f2()
    local f2a = {[1]=0, [2]=0}
    local f2i = 1

    res = tr and fls

    while f2a[f2i] ~= nil and not res do
        f2i = f2i + f1(0, 1)
        print("f2i=", f2i)
    end
    return f2i
end

local r = f2()
print(r)